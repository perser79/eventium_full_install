<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

if (getenv('APPLICATION_ENV') == 'local') {

  // ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
  /** El nombre de tu base de datos de WordPress */
  define('DB_NAME', 'eventium');

  /** Tu nombre de usuario de MySQL */
  define('DB_USER', 'root');

  /** Tu contraseña de MySQL */
  define('DB_PASSWORD', 'root');

  /** Host de MySQL (es muy probable que no necesites cambiarlo) */
  define('DB_HOST', 'localhost');

  /** Codificación de caracteres para la base de datos. */
  define('DB_CHARSET', 'utf8');

  /** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
  define('DB_COLLATE', '');

} else {

  // ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
  /** El nombre de tu base de datos de WordPress */
  define('DB_NAME', 'mjserrano_com');

  /** Tu nombre de usuario de MySQL */
  define('DB_USER', 'mjserrano_com');

  /** Tu contraseña de MySQL */
  define('DB_PASSWORD', 'Fja9JQeN');

  /** Host de MySQL (es muy probable que no necesites cambiarlo) */
  define('DB_HOST', 'mjserrano.com.mysql');

  /** Codificación de caracteres para la base de datos. */
  define('DB_CHARSET', 'utf8');

  /** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
  define('DB_COLLATE', '');

}

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'LOK<[v3wzYH5PYe|sAAi!=m9|Yy?O@PJD$OH1t -ch24oOL_prbKm5MLV;cHc,LM');
define('SECURE_AUTH_KEY', '`(`N=-0- y*<(f:!(%|tu5yt!i{cCpb.~8=}|=1Uzk37-hJp_aX*DdRJe6rYf)N!');
define('LOGGED_IN_KEY', 'Yt.*nO;bM&G/O[f3]FJhwVh>?A1{-KvD5yAnNIW;SV8bg0%%sPRVn2:brZ^<W4{ ');
define('NONCE_KEY', 'Oa;P;EYr&W<N:h79b/1Z6M{R-F9$Y7OM/&U1$:dNX%!r(u+WuAodKu:NW61;#iNZ');
define('AUTH_SALT', 'sOo 6iv]lnd8V2,(I*0etbz2`{<m1r5G6q4nP1uV#A>|mS+|=,ndR41(=T[tx=Q#');
define('SECURE_AUTH_SALT', 'hKdV7cKHLN40prwYIBP$#A3#,|0(<@p/^{L%zk3cvkt8/t0I4-2$v$Zv-x.#d_`H');
define('LOGGED_IN_SALT', 'A|Tl,W9KEzV?:@r&w,RWFdsTY9+^`qvh[5Ow+N|:]EEw$J%9o+uZlBLJV=.%K ^,');
define('NONCE_SALT', 'K[?>!$|@l1%PlM6>z+n_e}+4` 3}/_&h[R@aB_3)^B1ICXCkWZ)/SdmR~M:P++G9');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
if (getenv('APPLICATION_ENV') == 'local') {
  define('WP_DEBUG', true);
} else {
  define('WP_DEBUG', false);
}

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

